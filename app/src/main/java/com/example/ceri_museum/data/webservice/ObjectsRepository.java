package com.example.ceri_museum.data.webservice;

import java.util.Objects;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.moshi.MoshiConverterFactory;

public class ObjectsRepository {
    private static volatile ObjectsRepository INSTANCE;
    private final LiaInterface api;

    public synchronized static ObjectsRepository get() {
        if (INSTANCE == null) {
            INSTANCE = new ObjectsRepository();
        }

        return INSTANCE;
    }

    private ObjectsRepository() {
        Retrofit retrofit=
                new Retrofit.Builder()
                        .baseUrl("https://demo-lia.univ-avignon.fr/cerimuseum/")
                        .addConverterFactory(MoshiConverterFactory.create())
                        .build();

        api = retrofit.create(LiaInterface.class);
    }
}
