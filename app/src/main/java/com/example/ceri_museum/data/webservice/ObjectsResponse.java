package com.example.ceri_museum.data.webservice;

public class ObjectsResponse {

    public String name;
    public String [] categories;
    public String description;
    public int [] timeFrame;
    public Object properties;

    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name = name;
    }
}
