package com.example.ceri_museum.data.webservice;


import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;

public interface LiaInterface {
    @Headers("Accept: application/geo+json")
    @GET("/items/{name}/{categories},{description},{timeFrame}")
    Call<ObjectsResponse> getObject(@Path("name") String name,
                                      @Path("categories") String [] categories,
                                      @Path("description") String description,
                                      @Path("timeFrame") int [] timeFrame);
}
