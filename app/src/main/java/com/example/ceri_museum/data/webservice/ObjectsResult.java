package com.example.ceri_museum.data.webservice;

import com.example.ceri_museum.Categories;

import java.util.List;


import androidx.lifecycle.MutableLiveData;

public class ObjectsResult {
    public final boolean isLoading;
    public final List<Categories> categories;
    public final Throwable error;
    ObjectsResult(boolean isLoading, List<Categories> categories, Throwable error) {
        this.isLoading = isLoading;
        this.categories = categories;
        this.error = error;
    }
}
 /*class ObjectsRepository {
    public void load(String office, int gridX, int gridY) {
        final MutableLiveData<ObjectsResult> result =new MutableLiveData<>();
        result.setValue(new ObjectsResult(true,null,null));
    }
}*/
