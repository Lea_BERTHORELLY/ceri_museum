package com.example.ceri_museum.data.webservice;

public class Objects {
    public final String name;
    public final String [] categories;
    public final String description;
    public final int [] timeFrame;

    public Objects(String name, String [] categories,
                    String description, int [] timeFrame) {
        this.name = name;
        this.categories = categories;
        this.description = description;
        this.timeFrame = timeFrame;
    }
}
